package com.goswych.gc.api.sampleapp.kotlin.coroutine

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.Session
import com.goswych.gc.api.coroutine.GcClientApi
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {
    private val TAG = javaClass.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         * Inject keys to the lib
         */
        Session.setApiKeys(BuildConfig.swych_api_keys)


        btn1.setOnClickListener {
            openSession()
        }

        btn2.setOnClickListener {
            getCatalog()
        }

        btn3.setOnClickListener {
            addText("Order")
        }
    }

    fun openSession() {
        addText("openSession")
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val r = GcClientApi.openSession()
                withContext(Dispatchers.Main) {
                    addText("Access token received (${Session.accessTokenResult?.access_token?.count()})")
                }
            } catch (error: Exception) {
                withContext(Dispatchers.Main) {
                    onError(error)
                }
            }
        }
    }

    fun getCatalog() {
        addText("getCatalog")
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val r = GcClientApi.getCatalog()
                withContext(Dispatchers.Main) {
                    addText("Category: ${Session.catalogResult?.categoryList?.count()}\nCatalog: ${Session.catalogResult?.catalog?.count()}")
                }
            } catch (error: Exception) {
                withContext(Dispatchers.Main) {
                    onError(error)
                }
            }
        }
    }


    private fun addText(text: String) {
        Log.d(TAG, text)
        txt_result.text = "${txt_result.text}\n$text"
    }

    private val onError: (Throwable) -> Unit = errorHandler@{ errorThrowable ->
        addText("${errorThrowable.cause}")
        addText("${errorThrowable.message}")
        val accessTokenError = AccessTokenModel.parseError(errorThrowable) ?: return@errorHandler
        addText("${accessTokenError.error}\n${accessTokenError.error_description}")
    }
}
