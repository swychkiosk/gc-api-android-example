# gc-api-android-example
Sample app projects to use Swych GC android api (https://bitbucket.org/swychkiosk/gc-api-android).

- Kotlin app is located in ./kotlin
- Java app is located in ./java

# How to compile
1. Get api src (or pull latest). In terminal, run ```./get-api.sh```
2. To compile kotlin app:
```
cd kotlin/ && ./gradlew assemble && cd ../
```
3. To compile java app:
```
cd java/ && ./gradlew assemble && cd ../
```