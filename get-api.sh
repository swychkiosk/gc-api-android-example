#!/bin/bash
# Author: Trung Vo

repo=gc-api-android
urlPath=https://bitbucket.org/swychkiosk

cd ..

if [ -d "$repo" ]; then
  echo "- $repo exists => pulling latest"
  git -C ./$repo pull
  echo " "
else git clone $urlPath/$repo
fi