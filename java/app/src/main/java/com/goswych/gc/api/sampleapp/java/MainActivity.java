package com.goswych.gc.api.sampleapp.java;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.goswych.gc.api.AccessTokenModel;
import com.goswych.gc.api.GcClientApi;
import com.goswych.gc.api.Session;

import io.reactivex.disposables.Disposable;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView txt_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        /**
         * Inject keys to the lib
         */
        Session.setApiKeys(BuildConfig.swych_api_keys);

        Button btn1 = findViewById(R.id.btn1);
        Button btn2 = findViewById(R.id.btn2);
        Button btn3 = findViewById(R.id.btn3);
        txt_result = findViewById(R.id.txt_result);

        btn1.setOnClickListener(view -> {
            addText("openSession");
            disposable = GcClientApi.openSession(result -> {
                addText("Access token received ("+Session.getAccessTokenResult().getAccess_token().length()+")");
                return null;
            }, onError);
            if (disposable == null) addText("Session already opened");
        });

        btn2.setOnClickListener(view -> {
            addText("getCatalog");
            disposable = GcClientApi.getCatalog(result -> {
                addText("Category: "+Session.getCatalogResult().getCategoryList().length+"\nCatalog: "+Session.getCatalogResult().getCatalog().length);
                return null;
            }, onError, null, null, null);
        });

        btn3.setOnClickListener(view -> {
            addText("Order");
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Disposable disposable = null;

    @Override
    protected void onPause() {
        super.onPause();
        if (disposable != null) disposable.dispose();
    }

    void addText(String text) {
        txt_result.setText(txt_result.getText() + "\n" + text);
    }

    Function1<Throwable, Unit> onError = errorThrowable -> {
        addText(errorThrowable.getMessage());
        AccessTokenModel.Error accessTokenError = AccessTokenModel.parseError(errorThrowable);
        if (accessTokenError == null) return null;
        addText(accessTokenError.getError()+"\n"+accessTokenError.getError_description());
        return null;
    };
}
